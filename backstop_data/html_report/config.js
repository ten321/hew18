report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew18_test_HEWeb_2018_Homepage_0_document_0_phone.png",
        "test": "../bitmaps_test/20220814-104218/backstop_hew18_test_HEWeb_2018_Homepage_0_document_0_phone.png",
        "selector": "document",
        "fileName": "backstop_hew18_test_HEWeb_2018_Homepage_0_document_0_phone.png",
        "label": "HEWeb 2018 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hew18.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew18_test_HEWeb_2018_Homepage_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220814-104218/backstop_hew18_test_HEWeb_2018_Homepage_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "backstop_hew18_test_HEWeb_2018_Homepage_0_document_1_tablet.png",
        "label": "HEWeb 2018 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hew18.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew18_test_HEWeb_2018_Homepage_0_document_2_desktop.png",
        "test": "../bitmaps_test/20220814-104218/backstop_hew18_test_HEWeb_2018_Homepage_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "backstop_hew18_test_HEWeb_2018_Homepage_0_document_2_desktop.png",
        "label": "HEWeb 2018 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hew18.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.07",
          "analysisTime": 88
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew18_test_HEWeb_2018_Schedule_0_document_0_phone.png",
        "test": "../bitmaps_test/20220814-104218/backstop_hew18_test_HEWeb_2018_Schedule_0_document_0_phone.png",
        "selector": "document",
        "fileName": "backstop_hew18_test_HEWeb_2018_Schedule_0_document_0_phone.png",
        "label": "HEWeb 2018 Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hew18.pantheonsite.io/schedule/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew18_test_HEWeb_2018_Schedule_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220814-104218/backstop_hew18_test_HEWeb_2018_Schedule_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "backstop_hew18_test_HEWeb_2018_Schedule_0_document_1_tablet.png",
        "label": "HEWeb 2018 Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hew18.pantheonsite.io/schedule/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew18_test_HEWeb_2018_Schedule_0_document_2_desktop.png",
        "test": "../bitmaps_test/20220814-104218/backstop_hew18_test_HEWeb_2018_Schedule_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "backstop_hew18_test_HEWeb_2018_Schedule_0_document_2_desktop.png",
        "label": "HEWeb 2018 Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://test-hew18.pantheonsite.io/schedule/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "backstop_hew18_test"
});